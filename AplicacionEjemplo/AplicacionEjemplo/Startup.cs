﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AplicacionEjemplo.Startup))]
namespace AplicacionEjemplo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
